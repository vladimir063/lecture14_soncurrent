package org.sber.exercise1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatrixService {

    private final Object mutex1 = new Object();

    public int sum(int[][] matrix, int nthreads) throws InterruptedException {

        int delimeter = Math.round((float) matrix.length / nthreads);
        int count = matrix.length;
        List<MyThread> myThreads = new ArrayList<>();
        int lineNumber = 0;
        while (count > 0) {
            List<ColumnSummator> columnSummators = new ArrayList<>();
            if (count - delimeter < 1) {
                columnSummators.add(new ColumnSummator(matrix[lineNumber]));
                myThreads.add(new MyThread(columnSummators));
                break;
            }
            for (int i = 0; i < delimeter; i++) {
                columnSummators.add(new ColumnSummator(matrix[lineNumber]));
                lineNumber++;
            }
            myThreads.add(new MyThread(columnSummators));
            count = count - delimeter;
        }

        synchronized (mutex1) {
            myThreads.forEach(Thread::start);
            new Thread(() -> {
                waitFinishThreads(myThreads);
            }).start();
            mutex1.wait();
        }
        return myThreads.stream().mapToInt(MyThread::getSummLines).sum();
    }

    private void waitFinishThreads(List<MyThread> myThreads) {
        myThreads.stream().forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        synchronized (mutex1) {
            mutex1.notifyAll();
        }
    }


    class ColumnSummator implements Runnable {

        private final int[] line;
        private int lineSumm;

        public ColumnSummator(int[] line) {
            this.line = line;
        }

        public int getLineSumm() {
            return lineSumm;
        }

        @Override
        public void run() {
            lineSumm = lineSumm + Arrays.stream(line).sum();
        }
    }

    class MyThread extends Thread {

        private List<ColumnSummator> list;
        private int summLines;

        public MyThread(List<ColumnSummator> list) {
            this.list = list;
        }

        public int getSummLines() {
            return summLines;
        }

        @Override
        public void run() {
            list.forEach(ColumnSummator::run);
            summLines = list.stream().mapToInt(ColumnSummator::getLineSumm).sum();
        }
    }

}
